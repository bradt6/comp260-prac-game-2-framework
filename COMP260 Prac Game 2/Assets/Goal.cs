﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

	public AudioClip scoreClip;
	private AudioSource audio;

	void Start() {
		audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider collider) {
		// play score sound
		audio.PlayOneShot(scoreClip);
	}
}
