﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ai : MonoBehaviour {
	private Rigidbody rigidbody;
	public float speed = 5f;
	public Transform puck;


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false ;



	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 pos = new Vector3 (puck.position.x, puck.position.y);
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		float move = speed * Time.deltaTime;
		float disToTarget = dir.magnitude;

		if (move > disToTarget){
			vel = vel * disToTarget / move;
	}
		rigidbody.velocity = vel;
	
	}

	void update (){
	}
}