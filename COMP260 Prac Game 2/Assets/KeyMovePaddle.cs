﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class KeyMovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 30.0f;
	MovePaddle movePaddle;
	// Use this for initialization

	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		movePaddle = FindObjectOfType <MovePaddle> ();


	}


	void FixedUpdate () {
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		rigidbody.velocity = direction * speed;
	}

	// Update is called once per frame
	void Update () {

	}
}
